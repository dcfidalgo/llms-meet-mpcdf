<h1 align="center">
  <a href=""><img src="llms_meet_mpcdf.png" alt="llms meet mpcdf logo" width="210"></a>
  <br>
</h1>


# LLMs meet MPCDF

**This repository is work in progress!** 

The idea is to provide examples and guidance on how to deal with serving and fine-tuning LLMs at the MPCDF.

Ideas:
- Apptainer based
- Mainly Hugging Face based (transformers, accelerate, datasets, TRL, peft, TGI)
- (Q)LoRa
- FSDP
- DeepSpeed
- SFT, DPO, PPO

## Usage
Each folder contains a specific example/recipe.

### `ollama`
General example of how to use [ollama](https://github.com/ollama/ollama) to serve an open model, e.g. **llama3**, on the cluster.

### `sft_with_fsdp`
An example to fully supervise-fine-tune a 70B llama2 model using FSDP.

### `tgi`
General example of how to use [tgi](https://github.com/huggingface/text-generation-inference/) to serve an open model on the cluster.

### `sglang`
Example of how to run **DeepSeek-V3** on multiple nodes with [SGLang](https://github.com/sgl-project/sglang) on Viper.

## Glossary

- [Hugging Face](https://huggingface.co/): A company/community that provides a lot of open-source tools/libraries to work with machine learning models, especially NLP models.  
- [apptainer](https://apptainer.org/): *"Apptainer (formerly Singularity) simplifies the creation and execution of containers, ensuring software components are encapsulated for portability and reproducibility."* See the [MPCDF docs](https://docs.mpcdf.mpg.de/doc/computing/software/containers.html#apptainer) for usage examples on our HPC systems.
- [Supervised-Fine-Tuning (SFT)](): The continuation of the pre-training with the same "next-token-prediction" objective using a curated dataset.
- [Direct-Preference-Optimization](): ...
- [PPO](): ...
- [(FSDP)]():
- [(LoRA)](): ...
- [Quantization](): ...

## Resources

- [Simple and Scalable Strategies to Continually Pre-train Large Language Models *(paper)*](https://arxiv.org/pdf/2403.08763.pdf)

## Project status
Below is a list of examples/recipies we try to include in this repo.
If you would like to help covering these points, or if you are already using LLMs on our systems, we would be more than happy to hear from you :) 

- [x] Decribe how to access your inference server when running on an interactive/rvs node
- [x] Add inference examples for quantized models
- [ ] add vllm example for inference
- [ ] Add FSDP example
- [ ] Add DPO example
- [ ] Add LoRA/DoRA example, or some quantized variant of them

Feel free to contact us if you have ideas or use-cases that are not covered yet!

## Authors and acknowledgment
David Carreto Fidalgo and the whole AI Team at MPDCF
