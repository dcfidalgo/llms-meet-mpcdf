import argparse
import huggingface_hub
import openai


parser = argparse.ArgumentParser()
parser.add_argument(
    "--url",
    help="The URL to send the request to (default: http://localhost:9999).",
    default="http://localhost:9999",
)
args = parser.parse_args()


# using the Hugging Face Client (https://huggingface.co/docs/text-generation-inference/basic_tutorials/consuming_tgi)
client = huggingface_hub.InferenceClient(model=args.url)
response = client.text_generation(prompt="What is deep learning?")
print(response)

# using the OpenAI Python Client (https://github.com/openai/openai-python)
client = openai.OpenAI(base_url=f"{args.url}/v1", api_key="-")
chat_completion = client.chat.completions.create(
    model="tgi",
    messages=[
        {"role": "system", "content": "You are a helpful assistant."},
        {"role": "user", "content": "What is deep learning?"},
    ],
    stream=False,
)
print(chat_completion.choices[0].message.content)
