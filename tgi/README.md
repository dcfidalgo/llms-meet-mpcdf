# Text-Generation-Inference at the MPCDF

> *Text Generation Inference (TGI) is a toolkit for deploying and serving Large Language Models (LLMs). *
> *TGI enables high-performance text generation for the most popular open-source LLMs, including Llama, Falcon, StarCoder, BLOOM, GPT-NeoX, and T5.*

Check out their [documentation](https://huggingface.co/docs/text-generation-inference/index) or [GitHub repo](https://github.com/huggingface/text-generation-inference) for more information.

## Running TGI at the MPCDF

We run TGI via their official docker image converted to an apptainer:

```shell
module load apptainer/1.2.2
apptainer pull tgi.sif docker://ghcr.io/huggingface/text-generation-inference
```

We distinguish between **two ways** to use the LLM:

### Submit an inference job

The first way would be to submit a job on the cluster, in which we start the inference server and run a script that makes requests.
For running the request script, we will build another container where our clients of choice are installed:

```shell
apptainer build clients.sif clients.def
```

After customizing our `make_request.py` and the `run_llm_inference.slurm` script, we can submit the job:

```shell
sbatch run_llm_inference.slurm
```

> :warning: If you get an error "libcuda.so cannot found!", have a look at https://gitlab.mpcdf.mpg.de/dcfidalgo/llms-meet-mpcdf/-/issues/1#note_253525 for a workaround. This can happen if you shard the model on more than 2 GPUs, for example.

### Run the server interactively

A more interactive way to run the LLM, is to start a JupyterLab session via the [RVS](https://docs.mpcdf.mpg.de/doc/visualization/index.html#remote-visualization-and-jupyter-notebook-services), 
and start the inference server in a terminal within JupyterLab, for example:
```shell
module load apptainer/1.2.2
cache_for_model_weights=/ptmp/dcfidalgo/huggingface
apptainer run --nv -B ./:"$HOME",$cache_for_model_weights:/data \
  tgi.sif --model-id=TheBloke/Nous-Hermes-2-Mixtral-8x7B-DPO-AWQ --quantize=awq --port=9999
```

> :warning: Keep in mind, you can only access 1 GPU in this way.

You can then either:
- Interact with the LLM in the same JupyterLab session. Check out the [guide](https://gitlab.mpcdf.mpg.de/dataanalytics-public/containers#using-containers-with-rvs) on how to use the `clients` container as a Jupyter kernel.
- Or you can forward the port of the inference server to your local machine.
For this you need to figure out the node where the JupyterLab session is running (for example via `squeue --me` or by simply looking at the hostname in a JupyterLab terminal).
Using a [ControlMaster setup](https://docs.mpcdf.mpg.de/faq/tricks.html#how-can-i-avoid-having-to-type-my-password-repeatedly-how-can-i-tunnel-through-the-gateway-machines), you can then run following command on your local machine:

```shell
ssh -N -L 9999:<node name, e.g. ravg1002>:9999 <user name>@raven
```

Afterward, you should be able to run the `make_request.*` scripts from your local machine.
