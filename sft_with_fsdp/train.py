# coding=utf-8
# Copyright 2024 Sourab Mangrulkar. All rights reserved.
# Copyright 2024 David Carreto Fidalgo. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from dataclasses import dataclass, field
from typing import Optional

import datasets
from transformers import (
    HfArgumentParser,
    TrainingArguments,
    AutoModelForCausalLM,
    AutoTokenizer,
)
from transformers import set_seed
from trl import SFTTrainer


# Define and parse arguments.
@dataclass
class Arguments:
    model_name_or_path: str = field(
        default="meta-llama/Llama-2-70b-hf",
        metadata={
            "help": "Path to pretrained model or model identifier from huggingface.co/models"
        },
    )
    dataset_path: Optional[str] = field(
        default=f"/ptmp/{os.environ['USER']}/dataset",
        metadata={"help": "Path to the downloaded dataset."},
    )
    dataset_text_field: str = field(
        default="content", metadata={"help": "Dataset field to use as input text."}
    )
    use_flash_attn: Optional[bool] = field(
        default=False,
        metadata={"help": "Enables Flash attention for training."},
    )
    max_seq_length: Optional[int] = field(
        default=512, metadata={"help": "Maximum sequence length of the training input."}
    )
    packing: Optional[bool] = field(
        default=True,
        metadata={
            "help": "Use a `ConstantLengthDataset` that packs shorter sequences to reach the `max_seq_length`."
        },
    )


def main(
    args: Arguments,
    training_args: TrainingArguments,
):
    # Set seed for reproducibility
    set_seed(training_args.seed)

    # model and tokenizer
    model = AutoModelForCausalLM.from_pretrained(
        args.model_name_or_path,
        trust_remote_code=True,
        attn_implementation="flash_attention_2" if args.use_flash_attn else "eager",
        torch_dtype="auto",
    )
    tokenizer = AutoTokenizer.from_pretrained(
        args.model_name_or_path, trust_remote_code=True
    )
    tokenizer.pad_token = tokenizer.eos_token

    # gradient ckpt
    model.config.use_cache = not training_args.gradient_checkpointing
    if training_args.gradient_checkpointing:
        training_args.gradient_checkpointing_kwargs = {"use_reentrant": False}

    # datasets
    train_dataset, eval_dataset = datasets.load_from_disk(args.dataset_path).values()

    # trainer
    trainer = SFTTrainer(
        model=model,
        tokenizer=tokenizer,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
        packing=args.packing,
        dataset_text_field=args.dataset_text_field,
        max_seq_length=args.max_seq_length,
    )
    trainer.accelerator.print(f"{trainer.model}")

    # train
    checkpoint = None
    if training_args.resume_from_checkpoint is not None:
        checkpoint = training_args.resume_from_checkpoint
    trainer.train(resume_from_checkpoint=checkpoint)

    # saving final model
    if trainer.is_fsdp_enabled:
        trainer.accelerator.state.fsdp_plugin.set_state_dict_type("FULL_STATE_DICT")
    trainer.save_model()


if __name__ == "__main__":
    parser = HfArgumentParser((Arguments, TrainingArguments))
    main(*parser.parse_args_into_dataclasses())
