#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./job.out.%j
#SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J sglang
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --gres=gpu:2
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# Wall clock limit (max. is 24 hours):
#SBATCH --time=01:00:00

module purge
module load apptainer/1.3.2

MODEL="deepseek-ai/DeepSeek-V3"

NNODES=4
TP_SIZE=8
HEAD_HOSTNAME="$(hostname)"
HEAD_IPADDRESS="$(hostname --ip-address)"
PORT=8998

# from: `apptainer pull docker://lmsysorg/sglang:v0.4.1.post4-rocm620`
SIF="sglang_v0.4.1.post4-rocm620.sif"


echo "########## Starting the server ... ##########"

apptainer_cmd="apptainer exec \
    -B /ptmp/$USER/huggingface:/root/.cache/huggingface \
    --env HF_HOME=/root/.cache/huggingface \
    --env HF_HUB_OFFLINE=1 \
    --env TOKENIZERS_PARALLELISM=false \
    --env OUTLINES_CACHE_DIR=\$JOB_SHMTMPDIR/outlines_cache \
    $SIF"

sglang_cmd="python3 -m sglang.launch_server \
    --model-path $MODEL \
    --disable-cuda-graph \
    --tp $TP_SIZE \
    --nccl-init-addr $HEAD_IPADDRESS:$PORT \
    --nnodes $NNODES \
    --node-rank \$SLURM_NODEID \
    --trust-remote-code"

srun -o "./server.log.%j.%n" bash -c "./monitor_gpu.sh > gpus_node\$SLURM_NODEID.log 2>&1 & $apptainer_cmd $sglang_cmd" &

apptainer exec $SIF python3 -c "from sglang.utils import wait_for_server; wait_for_server('http://localhost:30000')"


echo "########## Sending the request ... ##########"

curl http://localhost:30000/v1/chat/completions \
    -H "Content-Type: application/json" \
    -d '{"model": "'$MODEL'", "messages": [{"role": "user", "content": "What is the physics behind the blue sky?"}]}'

