#!/bin/bash

MODEL="deepseek-ai/DeepSeek-V3"
SIF="sglang_v0.4.1.post4-rocm620.sif"
HF_TOKEN="..."

mkdir /ptmp/$USER/huggingface
apptainer exec \
    -B /ptmp/$USER/huggingface:/root/.cache/huggingface \
    --env HF_HOME=/root/.cache/huggingface \
    --env HF_TOKEN=$HF_TOKEN \
    $SIF huggingface-cli download $MODEL

