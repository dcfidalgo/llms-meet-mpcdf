# Running DeepSeek-V3 on Viper

This folder contains an example slurm script of how to run DeepSeek-V3 on Viper GPU with SGLang.
You should get an performance of around ~6 token/s (batch size 1).

## Build container

```bash
module load apptainer/1.3.2
apptainer pull docker://lmsysorg/sglang:v0.4.1.post4-rocm620
```

## Download model

> Provide your HF token in the `download.sh` script!

```bash
bash download.sh
```

## Run server

```bash
sbatch sglang.slurm
```

