import argparse
import ollama
import openai

parser = argparse.ArgumentParser()
parser.add_argument(
    "--url",
    help="The URL to send the request to.",
    default="http://localhost:11434",
)
parser.add_argument(
    "--model",
    help="The model to run. Look at https://ollama.com/library for the names.",
    default="llama3",
)
args = parser.parse_args()

# pull the model if it is not cached
client = ollama.Client(host=args.url)
try:
    client.chat(args.model)
except ollama.ResponseError as e:
    print(e.error)
    if e.status_code == 404:
        client.pull(args.model)


messages = [
    {"role": "system", "content": "You are a helpful assistant."},
    {"role": "user", "content": "What is deep learning?"},
]

response = client.chat(
    model=args.model, stream=False, messages=messages, options={"num_predict": 20}
)
print("OLLAMA CLIENT:", response)

client = openai.OpenAI(base_url=f"{args.url}/v1", api_key="ollama")
chat_completion = client.chat.completions.create(
    model=args.model,
    messages=messages,
    stream=False,
    max_tokens=20,
)
print("OPENAI CLIENT", chat_completion)
