# ollama at the MPCDF

> *Get up and running with large language models locally.*
>
> *Run Llama 3, Mistral, Gemma, and other models. Customize and create your own.*

Ollama runs and serves LLMs in [GGUF](https://huggingface.co/docs/hub/gguf) format and uses [llama.cpp](https://github.com/ggerganov/llama.cpp) as backend.
Check out their [repo](https://github.com/ollama/ollama) and their [model list](https://ollama.com/library).

## Running ollama at the MPCDF

We run ollama via their official docker image converted to an apptainer:

```shell
module load apptainer/1.2.2
apptainer pull ollama.sif docker://ollama/ollama
```

We distinguish between **two ways** to use the LLM:

### Submit an inference job

The first way would be to submit a job on the cluster, in which we start the inference server and run a script that makes requests.
For running the request script, we will build another container where our clients of choice are installed:

```shell
apptainer build clients.sif clients.def
```

After customizing our `make_request.py` and the `run_llm_inference.slurm` script, we can submit the job:

```shell
sbatch run_llm_inference.slurm
```

### Run the server interactively

A more interactive way to run the LLM, is to start a JupyterLab session via the [RVS](https://docs.mpcdf.mpg.de/doc/visualization/index.html#remote-visualization-and-jupyter-notebook-services),
and start the inference server in a terminal within JupyterLab, for example:
```shell
module load apptainer/1.2.2
# where to store the downloaded models
export OLLAMA_MODELS=/ptmp/dcfidalgo/ollama
apptainer run --nv -B .:"$HOME",$OLLAMA_MODELS ollama.sif
```

> :warning: Keep in mind, you can only access 1 GPU in this way.

You can then either:
- Interact with the LLM in the same JupyterLab session. Check out the [guide](https://gitlab.mpcdf.mpg.de/dataanalytics-public/containers#using-containers-with-rvs) on how to use the `clients` container as a Jupyter kernel.
- Or you can forward the port of the inference server to your local machine.
  For this you need to figure out the node where the JupyterLab session is running (for example via `squeue --me` or by simply looking at the hostname in a JupyterLab terminal).
  Using a [ControlMaster setup](https://docs.mpcdf.mpg.de/faq/tricks.html#how-can-i-avoid-having-to-type-my-password-repeatedly-how-can-i-tunnel-through-the-gateway-machines), you can then run following command on your local machine:

```shell
ssh -N -L 11434:<node name, e.g. ravg1002>:11434 <user name>@raven
```

Afterward, you should be able to run the `make_request.py` script from your local machine.
