ADDRESS=$1

apptainer exec -B .:$PWD vllm.sif amd-smi monitor -w 1 -u -m -p -t -v --file amdsmi_$SLURMD_NODENAME.log > /dev/null &
apptainer exec -B /ptmp/dcfidalgo/huggingface:/root/.cache/huggingface --env HF_HOME=/root/.cache/huggingface --env HF_HUB_OFFLINE=1 --env RAY_EXPERIMENTAL_NOSET_ROCR_VISIBLE_DEVICES=1 --env TOKENIZERS_PARALLELISM=false vllm.sif ray start --block --address=$ADDRESS
