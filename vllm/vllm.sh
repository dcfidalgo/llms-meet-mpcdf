#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./job.out.%j
#SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J vllm
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --gres=gpu:2
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# Wall clock limit (max. is 24 hours):
#SBATCH --time=00:30:00

module purge
module load apptainer

MODEL="amd/Llama-3.1-405B-Instruct-FP8-KV"
HEAD_HOSTNAME="$(hostname)"
HEAD_IPADDRESS="$(hostname --ip-address)"
PORT=6379

#amd-smi monitor -w 1 -u -m -p -t > amdsmi_node0.log

echo "Starting head node on $HEAD_HOSTNAME ..."
srun -J "head node" -N 1 -c $(( SLURM_CPUS_ON_NODE/2 )) -w $HEAD_HOSTNAME apptainer exec -B /ptmp/dcfidalgo/huggingface:/root/.cache/huggingface --env HF_HOME=/root/.cache/huggingface --env HF_HUB_OFFLINE=1 --env RAY_EXPERIMENTAL_NOSET_ROCR_VISIBLE_DEVICES=1 --env TOKENIZERS_PARALLELISM=false vllm.sif ray start --block --head --port=$PORT --dashboard-host=0.0.0.0 &
sleep 10

echo "Starting worker nodes ..."
srun -J "worker node" -N $(( SLURM_NNODES-1 )) -x $HEAD_HOSTNAME bash ./worker.sh "$HEAD_IPADDRESS:$PORT" &
sleep 30

# echo "Starting server"
apptainer exec -B .:$PWD vllm.sif amd-smi monitor -w 1 -u -m -p -t -v --file amdsmi_$SLURMD_NODENAME.log > /dev/null &
apptainer exec -B /ptmp/dcfidalgo/huggingface:/root/.cache/huggingface --env HF_HOME=/root/.cache/huggingface --env HF_HUB_OFFLINE=1 --env RAY_EXPERIMENTAL_NOSET_ROCR_VISIBLE_DEVICES=1 --env TOKENIZERS_PARALLELISM=false vllm.sif python3 -m vllm.entrypoints.openai.api_server --model=$MODEL --pipeline-parallel-size=$SLURM_JOB_NUM_NODES --tensor-parallel-size=2 --distributed-executor-backend="ray" --dtype float16 --swap-space 16 --quantization fp8 --kv-cache-dtype fp8 --max_model_len 1000 --gpu-memory-utilization 0.7 --enforce-eager > server.log 2>&1 &

until [ $(curl -sL -w "%{http_code}" localhost:8000/health -o /dev/null) -eq "200" ]; do
    sleep 3
    echo "[INFO] Waiting for localhost:8000 to be up and running"
done

curl http://localhost:8000/v1/chat/completions \
	-H "Content-Type: application/json" \
	-d '{"model": "'$MODEL'", "messages": [{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": "What is the physics behind the blue sky?"}]}'

apptainer exec vllm.sif ray stop

